public interface IntegerParser {

    /**
     * Returns an {@code Integer} object holding the numeric value of the specified {@code String}.
     * The argument is interpreted as representing an unsigned integer, exactly as if the argument was passed
     * to the {@link Integer#parseInt(java.lang.String)} method.
     *
     * For the purpose of this interview avoid using existing libraries or methods such as
     * <ul>    
     *     <li>{@link Integer#parseInt(java.lang.String)}</li>
     *     <li>regular expressions<li>
     * </ul>
     *
     * @param value The string to be parsed. Not null.
     * @return {@code Integer} object that represents the unsigned integer value specified by the string.
     * @throws IllegalArgumentException  if the string doesn't represent integer or the result would not fit in integer data type (input is bigger than max integer).
     */
    Integer parse(final String value);

}
