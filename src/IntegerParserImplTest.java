import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntegerParserImplTest {

    private static final IntegerParser PARSER = new IntegerParserImpl();

    @Test
    public void testParseInt_middle_range_num(){
        final String numStr = "100457";
        final Integer value = PARSER.parse(numStr);
        assertEquals(100457L, (long)value);
    }

    @Test
    public void testParseInt_little_number(){
        final String numStr = "1";
        final Integer value = PARSER.parse(numStr);
        assertEquals(1L, (long)value);
    }

    @Test
    public void testParseInt_zero(){
        final String numStr = "0";
        final Integer value = PARSER.parse(numStr);
        assertEquals(0L, (long)value);
    }

    @Test
    public void testParseInt_max_integer(){
        final String numStr = Integer.valueOf(Integer.MAX_VALUE).toString();
        final Integer value = PARSER.parse(numStr);
        assertEquals(2147483647L, (long)value);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseInt_integer_overflow(){
        final String numStr = "3147483647";
        PARSER.parse(numStr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseInt_integer_overflow_2(){
        final String numStr = "99993147483647";
        PARSER.parse(numStr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseInt_integer_overflow_3(){
        final String numStr = "2147483650";
        PARSER.parse(numStr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseInt_not_a_number(){
        final String numStr = "ABC";
        PARSER.parse(numStr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseInt_empty_input(){
        final String numStr = "";
        PARSER.parse(numStr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseInt_null_input(){
        PARSER.parse(null);
    }

}